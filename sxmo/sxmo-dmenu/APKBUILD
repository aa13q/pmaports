# Maintainer: Miles Alan <m@milesalan.com>
pkgname=sxmo-dmenu
pkgver=5.0.8
pkgrel=0
pkgdesc="Dmenu fork for Sxmo UI; supports highlight, centering, volume-key navigation and more"
url="https://git.sr.ht/~mil/sxmo-dmenu"
arch="all"
license="MIT"
makedepends="libx11-dev libxinerama-dev libxft-dev"
options="!check" # has no tests
subpackages="$pkgname-doc"
provides="dmenu"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~mil/sxmo-dmenu/archive/$pkgver.tar.gz"

prepare() {
	default_prepare
	sed -i -e '/CFLAGS/{s/-Os//;s/=/+=/}' \
		-e '/LDFLAGS/{s/=/+=/}' \
		"$builddir"/config.mk
}

build() {
	make X11INC=/usr/include/X11 \
		X11LIB=/usr/lib/X11 \
		FREETYPEINC=/usr/include/freetype2 \
		-C "$builddir"
}

package() {
	make DESTDIR=$pkgdir PREFIX=/usr \
		-C "$builddir" install
}

sha512sums="ccd5b914b5f8dcdf9220255c943932a43ce4b6cc9d68a20573ecab17a37f576f2c7072547d251654c7782a0e98cf2d87162c881a70bb2ac35d7dabcdce86c987  sxmo-dmenu-5.0.8.tar.gz"
